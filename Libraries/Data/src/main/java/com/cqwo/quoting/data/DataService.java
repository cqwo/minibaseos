package com.cqwo.quoting.data;

import com.cqwo.quoting.core.cache.CWMCache;
import com.cqwo.quoting.core.config.CWMConfig;
import com.cqwo.quoting.core.data.CWMData;
import com.cqwo.quoting.core.sms.CWMSMS;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;


@Getter
@Setter
public class DataService {

    @Autowired
    private CWMCache cwmCache;

    @Autowired
    private CWMData cwmData;

    @Autowired
    private CWMSMS cwmSMS;

    @Autowired
    private CWMConfig cwmConfig;


}
