package com.cqwo.quoting.core.data.rdbs.repository.users;

import com.cqwo.quoting.core.data.rdbs.repository.BaseRepository;
import com.cqwo.quoting.core.domain.users.UserRankInfo;

public interface UserRankRepository extends BaseRepository<UserRankInfo, Integer> {
}

