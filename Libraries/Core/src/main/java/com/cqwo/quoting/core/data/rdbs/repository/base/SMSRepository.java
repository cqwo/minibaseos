package com.cqwo.quoting.core.data.rdbs.repository.base;

import com.cqwo.quoting.core.data.rdbs.repository.BaseRepository;
import com.cqwo.quoting.core.domain.sms.SMSInfo;

public interface SMSRepository extends BaseRepository<SMSInfo, Integer> {
}
