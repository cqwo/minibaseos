package com.cqwo.quoting.core.data.rdbs.repository.base;

import com.cqwo.quoting.core.data.rdbs.repository.BaseRepository;
import com.cqwo.quoting.core.domain.users.Person;

public interface PersonRepository extends BaseRepository<Person, Integer> {


}
