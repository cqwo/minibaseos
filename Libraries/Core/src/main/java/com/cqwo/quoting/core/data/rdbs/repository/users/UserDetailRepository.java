package com.cqwo.quoting.core.data.rdbs.repository.users;

import com.cqwo.quoting.core.data.rdbs.repository.BaseRepository;
import com.cqwo.quoting.core.domain.users.UserDetailInfo;

public interface UserDetailRepository extends BaseRepository<UserDetailInfo, Integer> {

}