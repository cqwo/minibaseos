
package com.cqwo.quoting.core.data.rdbs.repository.authors;

import com.cqwo.quoting.core.data.rdbs.repository.BaseRepository;
import com.cqwo.quoting.core.domain.authors.AuthorSessionInfo;

public interface AuthorSessionRepository extends BaseRepository<AuthorSessionInfo, Integer> {
}