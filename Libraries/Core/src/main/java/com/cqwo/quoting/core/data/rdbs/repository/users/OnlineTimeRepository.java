package com.cqwo.quoting.core.data.rdbs.repository.users;

import com.cqwo.quoting.core.data.rdbs.repository.BaseRepository;
import com.cqwo.quoting.core.domain.users.OnlineTimeInfo;

public interface OnlineTimeRepository extends BaseRepository<OnlineTimeInfo, Integer> {
}
