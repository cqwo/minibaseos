
package com.cqwo.quoting.core.data.rdbs.repository.base;

import com.cqwo.quoting.core.data.rdbs.repository.BaseRepository;
import com.cqwo.quoting.core.domain.base.AttachmentInfo;

public interface AttachmentRepository extends BaseRepository<AttachmentInfo, Integer> {


}