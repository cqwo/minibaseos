
package com.cqwo.quoting.core.data.rdbs.repository.authors;

import com.cqwo.quoting.core.data.rdbs.repository.BaseRepository;
import com.cqwo.quoting.core.domain.authors.AuthorPermissionInfo;

public interface AuthorPermissionRepository extends BaseRepository<AuthorPermissionInfo, Integer> {
}