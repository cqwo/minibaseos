package com.cqwo.quoting.core.data.rdbs.repository.authors;

import com.cqwo.quoting.core.data.rdbs.repository.BaseRepository;
import com.cqwo.quoting.core.domain.authors.AuthorLogInfo;

public interface AuthorLogRepository extends BaseRepository<AuthorLogInfo, Integer> {




}
